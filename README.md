
# FIT UAF Lab
Guest lecture exploring use-after-free vulnerabilities and exploitation. Resources include source code and binaries referenced in the lecture, as well as the assigned lab binaries for this week.

## Solutions
While teamwork and collaboration is encouraged, please do not share solutions or code. You should individually submit your own automated solution for both protions of the lab.

Please avoid publicly posting solutions, even after completion of the course.

## Lecture Slides
Slides should eventually be posted [here](https://giphy.com/embed/zUzt6LNO9uMJW)

## Lab Assignments

### Lab 1 - C Struct UAF Exploitation
Very similar to the example in the lecture. Source code is available for assistance. A pre-built binary is included and your automated exploit should work against this binary. Binary should have all mitigations, including ASLR and stack canaries (Makefile is included). Confirm you have enabled ASLR in Linux by running `cat /proc/sys/kernel/randomize_va_space` (result should be `2`). If it is not, you can enable it by running `echo 2 > /proc/sys/kernel/randomize_va_space` as the root user.

**If your solution does not work with all mitigations enabled, it will be considered incomplete**

### Lab 2 - C++ Object UAF Exploitation
Exploitation of UAF C++ object, as discussed at the end of the lecture slides. Understanding of vtables/vptr is required. ASLR is not required for this lab, and should be disabled (value of 0 in /proc/sys/kernel/randomize_va_space)

###End