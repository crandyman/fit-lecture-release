/* doom doesn't mess around with his gcc protections, everything is on! */
/* compiled with: gcc -z relro -z now -fPIE -pie -fstack-protector-all -o heap_uaf heap_uaf.c */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

struct output_object {
    void (* message)(char *);
    char buffer[20];
};

void print_stdout(char *s)
{
    printf("%s\n", s);
}

void secret_shell()
{
    execve("/bin/sh", NULL, NULL);
}

void print_menu()
{
    printf("-- Menu ----------------------\n"
           "1. Make output object\n"
           "2. Make string\n"
           "3. Delete output object\n"
           "4. Delete string\n"
           "5. Print output object\n"
           "6. Print string\n"
           "7. Quit\n"
           "Enter Choice: ");
}

/* bugs galore... but no memory corruption! */
int main(int argc, char * argv[])
{
    setbuf(stdin, NULL);
    struct output_object *myobj = NULL;
    char *mystring = NULL;
    int choice = 0;

    while(1)
    {
        print_menu();
        
        /* get menu option */
        scanf("%u", &choice);
        getc(stdin);

        /* perform menu actions */
        if(choice == 1)
        {
            myobj = malloc(sizeof(struct output_object));
            printf("New output_object is at 0x%08x\n", (unsigned int)myobj);
            
            printf("Input message to output: ");
            fgets(myobj->buffer, 20, stdin);
            myobj->buffer[strcspn(myobj->buffer, "\n")] = 0;
            myobj->message = &print_stdout;

            /* yay */
            printf("Created new output_object!\n");
        }
        else if(choice == 2)
        {
            unsigned int str_size = 0;
            printf("How many bytes do you need in your string? ");
            if (scanf("%u", &str_size) != 1) {
              printf("Error reading input");
              exit(1);
            }
            // Consume newline
            getchar();

            if (str_size > 100) {
              printf("I find that to be an unreasonable length\n");
              exit(1);
            }

            mystring = malloc(str_size);
            memset(mystring, 0, str_size);
            printf("New string is at 0x%08x\n", (unsigned int)mystring);

            printf("What is your string? ");
            for (unsigned int i = 0; ; i++) {
              mystring[i] = getchar();
              // Break when '\n' received or hit str size limit
              if (mystring[i] == '\n' || i == str_size - 1) {
                mystring[i] = NULL;
                break;
              }
            }
        }
        else if(choice == 3)
        {
            if(myobj)
            {
                free(myobj);
                printf("Deleted myobj!\n");
            }
            else
                printf("There is no object to free!\n");
        }
        else if(choice == 4)
        {
            if(mystring)
            {
                free(mystring);
                printf("Deleted string!\n");
            }
            else
                printf("There is no string to free!\n");
        }
        else if(choice == 5)
        {
            if(myobj)
                myobj->message(myobj->buffer);
            else
                printf("There is no output object to print the message!\n");
        }
        else if(choice == 6)
        {
            if(mystring)
            {
                printf("%s", mystring);
            }
            else
                printf("There is no string to print!\n");
        }
        else if(choice == 7)
            break;
        else
            printf("Invalid choice!\n");
      
        choice = 0;
    }

    printf("See you tomorrow!\n");
    return 0;
}
