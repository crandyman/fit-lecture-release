#include <fcntl.h>
#include <iostream> 
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <stdio.h>
using namespace std;

class Employee{
private:
  // Debug function. Don't worry, it's not reachable ;)
  virtual void give_shell(){
    system("/bin/sh");
  }
protected:
  int age;
  string name;
public:
  virtual void printSummary(){
    cout << "Employee Name: " << name << endl;
    cout << "Employee Age: " << age << endl;
  }
};

class HourlyEmployee: public Employee{
protected:
  double hourlyRate;
public:
  HourlyEmployee(string name, int age, double hourlyRate){
    this->name = name;
    this->age = age;
    this->hourlyRate = hourlyRate;
  }

  virtual void printSummary(){
    Employee::printSummary();
    cout << "Hourly rate: " << hourlyRate << endl;
  }
};

class SalaryEmployee: public Employee{
protected:
  int salary;
public:
  SalaryEmployee(string name, int age, int salary){
    this->name = name;
    this->age = age;
    this->salary = salary;
  }
  virtual void printSummary(){
    Employee::printSummary();
    cout << "Salary: " << salary << endl;
  }
};

int main(int argc, char* argv[]){
  Employee* h = new HourlyEmployee("Jack", 25, 23.50);
  Employee* s = new SalaryEmployee("Jill", 21, 90000);

  size_t len;
  char* data;
  unsigned int op;
  while(1){
    cout << "1. Print employees\n2. Allocate stuff\n3. Free employees\n";
    cin >> op;

    switch(op){
      case 1:
        h->printSummary();
        s->printSummary();
        break;
      case 2:
        len = atoi(getenv("ALLOC_LEN"));
        data = new char[len];
        read(open(argv[1], O_RDONLY), data, len);
        cout << "your data is allocated" << endl;
        break;
      case 3:
        delete h;
        delete s;
        break;
      default:
        break;
    }
  }

  return 0;  
}
