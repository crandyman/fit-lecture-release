#include <iostream>

using namespace std;

class Animal {
  public:
    virtual void speak() {
      cout << "..." << endl;
    }

    void breathe() {
      cout << "In... and out..." << endl;
    }
};

class Cat : public Animal {
  public:
    virtual void speak() {
      cout << "Meow meow, hooman" << endl;
    }

    void scratchCurtains() {
      cout << "You didn't like them that much anyway" << endl;
    }
    
    void meowAllNight() {
      cout << "That is how a cat do" << endl;
    }
};

class Dog : public Animal {
  public:
    virtual void speak() {
      cout << "much bark, so woof" << endl;
    }

    void slobberConstantly() {
      cout << "Seriously, you should get that checked out" << endl;
    }
    
    void attackMailman() {
      cout << "That is how a dog do" << endl;
    }
};

int main(int argc, char *argv[]) {
  Animal *a1 = new Dog();
  Animal *a2 = new Cat();
  Dog d;
  Cat c;

  a1->speak();
  a2->speak();
  a1->breathe();
  a2->breathe();
  d.speak();
  c.speak();

  d.slobberConstantly();
  c.meowAllNight();
}
